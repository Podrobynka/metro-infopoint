require 'yaml'
require 'rgl/adjacency'
require 'rgl/traversal'
require 'rgl/dot'
require 'rgl/dijkstra'

# MetroInfopoint
class MetroInfopoint
  attr_reader :timing_data, :graph, :gr_data

  def initialize(
    path_to_timing_file: './config/timing2.yml',
    path_to_lines_file: './config/config.yml'
  )
    @path_to_lines_file = path_to_lines_file
    @timing_data = YAML.load_file(path_to_timing_file)['timing']

    @gr_data = @timing_data
               .map { |a| { [a['start'], a['end']] => a['time'] } }
               .reduce({}, :merge)

    @graph = RGL::DirectedAdjacencyGraph.new

    @gr_data.each { |(stop1, stop2), _v| @graph.add_edge(stop1, stop2) }

    @graph = @graph.to_undirected
  end

  def calculate(from_station:, to_station:)
    {
      price:
        calculate_price(from_station: from_station, to_station: to_station),
      time:
        calculate_time(from_station: from_station, to_station: to_station)
    }
  end

  def calculate_price(from_station:, to_station:)
    calculate_sum(
      paths: shortest_path(from_station: from_station, to_station: to_station),
      type: 'price'
    )
  end

  def calculate_time(from_station:, to_station:)
    calculate_sum(
      paths: shortest_path(from_station: from_station, to_station: to_station),
      type: 'time'
    )
  end

  private

  def shortest_path(from_station:, to_station:)
    from_station = from_station.to_sym
    to_station = to_station.to_sym
    stations = graph.dijkstra_shortest_path(gr_data, from_station, to_station)

    paths = []

    stations.each_with_index { |s, index| paths << [s, stations[index + 1]] }
    paths.delete_at(-1)

    paths
  end

  def calculate_sum(paths:, type:)
    sum = 0

    paths.each do |path|
      sum += timing_data.find do |e|
        e['start'].to_s == path[0].to_s &&
          e['end'].to_s == path[1].to_s ||
          e['start'].to_s == path[1].to_s &&
            e['end'].to_s == path[0].to_s
      end[type]
    end

    sum
  end
end
